import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ClrIconModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { NavComponent } from './layout/nav/nav.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';

@NgModule({
  declarations: [AppComponent, NavComponent, ContentLayoutComponent],
  imports: [BrowserModule, BrowserAnimationsModule, SharedModule, AppRoutingModule, RouterModule, ClrIconModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
