import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Link, NavbarService } from '../../shared/services/navbar/navbar.service';

@Component({
  selector: 'dbms-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  public links$: Observable<Array<Link>>;

  public subnavLinks$: Observable<Array<Link>>;

  public navbarVisible$: Observable<boolean>;

  public subnavVisible$: Observable<boolean>;

  public constructor(private navBarService: NavbarService) {}

  public ngOnInit(): void {
    this.links$ = this.navBarService.getNavLinks();
    this.subnavLinks$ = this.navBarService.getSubnavLinks();
    this.navbarVisible$ = this.navBarService.subscribeToNavVisibility();
    this.subnavVisible$ = this.navBarService.subscribeToSubNavVisibility();
  }
}
