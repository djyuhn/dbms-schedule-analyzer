import { OperationType } from '../models/OperationType';
import { Operation } from '../models/Operation';
import { ItemOperation } from '../models/ItemOperation';
import { isItemOperation } from './IsItemOperation';

describe('IsItemOperation', () => {
  it('should return true if operation is of type ItemOperation', () => {
    const operation: ItemOperation = {
      transactionId: 1,
      operationType: OperationType.Read,
      item: 1,
    };

    const result = isItemOperation(operation);

    expect(result).toBeTrue();
  });

  it('should return false if operation is not of type ItemOperation', () => {
    const operation: Operation = {
      transactionId: 1,
      operationType: OperationType.Read,
    };

    const result = isItemOperation(operation);

    expect(result).toBeFalse();
  });
});
