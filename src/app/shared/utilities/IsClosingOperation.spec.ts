import { ClosingOperation } from '../models/ClosingOperation';
import { OperationType } from '../models/OperationType';
import { isClosingOperation } from './IsClosingOperation';
import { Operation } from '../models/Operation';

describe('IsClosingOperation', () => {
  it('should return true if operation is of type ClosingOperation with a Commit', () => {
    const operation: ClosingOperation = {
      transactionId: 1,
      operationType: OperationType.Commit,
    };

    const result = isClosingOperation(operation);

    expect(result).toBeTrue();
  });

  it('should return true if operation is of type ClosingOperation with an Abort', () => {
    const operation: ClosingOperation = {
      transactionId: 1,
      operationType: OperationType.Abort,
    };

    const result = isClosingOperation(operation);

    expect(result).toBeTrue();
  });

  it('should return false if operation is not of type ClosingOperation', () => {
    const operation: Operation = {
      transactionId: 1,
      operationType: OperationType.Read,
    };

    const result = isClosingOperation(operation);

    expect(result).toBeFalse();
  });
});
