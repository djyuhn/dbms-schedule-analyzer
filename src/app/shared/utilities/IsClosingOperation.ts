import { Operation } from '../models/Operation';
import { ClosingOperation } from '../models/ClosingOperation';
import { OperationType } from '../models/OperationType';

export const isClosingOperation = (operation: Operation): operation is ClosingOperation => {
  return (
    (operation as ClosingOperation).operationType === OperationType.Abort ||
    (operation as ClosingOperation).operationType === OperationType.Commit
  );
};
