import { Operation } from '../models/Operation';
import { ItemOperation } from '../models/ItemOperation';

export const isItemOperation = (operation: Operation): operation is ItemOperation => {
  return (operation as ItemOperation).item !== undefined;
};
