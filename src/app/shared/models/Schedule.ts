import { Operation } from './Operation';
import { Transaction } from './Transaction';
import { ClosingOperation } from './ClosingOperation';

export interface Schedule {
  transactionList: ReadonlyArray<Transaction>;
  operationSequence: ReadonlyArray<Operation>;
  closingOperationSequence: ReadonlyArray<ClosingOperation>;
}
