import { Operation } from './Operation';
import { OperationType } from './OperationType';

export interface ItemOperation extends Operation {
  item: number;
  operationType: OperationType.Read | OperationType.Write;
}
