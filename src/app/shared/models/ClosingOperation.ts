import { Operation } from './Operation';
import { OperationType } from './OperationType';

export interface ClosingOperation extends Operation {
  operationType: OperationType.Abort | OperationType.Commit;
}
