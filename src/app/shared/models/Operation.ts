import { OperationType } from './OperationType';

export interface Operation {
  transactionId: number;
  operationType: OperationType;
}
