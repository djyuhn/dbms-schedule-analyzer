export enum OperationType {
  Read = 'r',
  Write = 'w',
  Commit = 'c',
  Abort = 'a',
}
