import { Operation } from './Operation';

export interface ScheduleConflict {
  precedingIndex: number;
  precedingOperation: Operation;
  followingIndex: number;
  followingOperation: Operation;
}
