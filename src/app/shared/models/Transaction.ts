import { Operation } from './Operation';
import { ClosingOperation } from './ClosingOperation';

export interface Transaction {
  transactionId: number;
  operations: ReadonlyArray<Operation>;
  closingOperation: ClosingOperation;
}
