import { Injectable } from '@angular/core';
import { Graph, NodeId } from 'ngraph.graph';

@Injectable({
  providedIn: 'root',
})
export class GraphingService {
  public constructor() {}

  public graphHasCycle(graph: Graph): boolean {
    const discovered = new Set<NodeId>();
    const finished = new Set<NodeId>();

    const adjacencyList = this.createAdjacencyList(graph);

    for (const [key] of adjacencyList) {
      if (!discovered.has(key) && !finished.has(key)) {
        const hasCycle = this.depthFirstSearchVisit(adjacencyList, key, discovered, finished);
        if (hasCycle) {
          return hasCycle;
        }
      }
    }

    return false;
  }

  private createAdjacencyList(graph: Graph): Map<NodeId, Set<NodeId>> {
    const adjacencyList = new Map<NodeId, Set<NodeId>>();
    graph.forEachLink(link => {
      if (adjacencyList.get(link.fromId)) {
        adjacencyList.get(link.fromId).add(link.toId);
      } else {
        adjacencyList.set(
          link.fromId,
          new Set<NodeId>([link.toId]),
        );
      }
    });

    return adjacencyList;
  }

  private depthFirstSearchVisit(
    adjacencyList: Map<NodeId, Set<NodeId>>,
    transaction: NodeId,
    discovered: Set<NodeId>,
    finished: Set<NodeId>,
  ): boolean {
    discovered.add(transaction);

    const adjacentTransactions = adjacencyList.get(transaction);

    if (adjacentTransactions) {
      for (const adjTransaction of adjacentTransactions) {
        if (discovered.has(adjTransaction)) {
          return true;
        }
        if (!finished.has(adjTransaction)) {
          return this.depthFirstSearchVisit(adjacencyList, adjTransaction, discovered, finished);
        }
      }
    }

    discovered.delete(transaction);
    finished.add(transaction);
    return false;
  }
}
