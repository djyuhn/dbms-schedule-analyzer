import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavbarService {
  public constructor() {}

  private navLinks$: BehaviorSubject<Array<Link>> = new BehaviorSubject<Array<Link>>([]);

  private subNavLinks$: BehaviorSubject<Array<Link>> = new BehaviorSubject<Array<Link>>([]);

  private navVisible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  private subNavVisible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  public subscribeToNavVisibility(): Observable<boolean> {
    return this.navVisible$.asObservable();
  }

  public subscribeToSubNavVisibility(): Observable<boolean> {
    return this.subNavVisible$.asObservable();
  }

  public hideNav() {
    this.navVisible$.next(false);
  }

  public showNav() {
    this.navVisible$.next(true);
  }

  public hideSubNav() {
    this.subNavVisible$.next(false);
  }

  public showSubNav() {
    this.subNavVisible$.next(true);
  }

  public setNavLinks(links: Array<Link>) {
    this.navLinks$.next(links);
  }

  public getNavLinks() {
    return this.navLinks$.asObservable();
  }

  public setSubnavLinks(links: Array<Link>) {
    this.subNavLinks$.next(links);
  }

  public getSubnavLinks() {
    return this.subNavLinks$.asObservable();
  }
}

export interface Link {
  text: string;
  path: string;
}
