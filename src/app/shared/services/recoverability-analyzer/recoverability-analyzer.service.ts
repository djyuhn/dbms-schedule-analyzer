import { Injectable } from '@angular/core';
import { Schedule } from '../../models/Schedule';
import { Operation } from '../../models/Operation';
import { ClosingOperation } from '../../models/ClosingOperation';
import { OperationType } from '../../models/OperationType';
import { isClosingOperation, isItemOperation } from '../../utilities';

@Injectable({
  providedIn: 'root',
})
export class RecoverabilityAnalyzerService {
  public constructor() {}

  public isRecoverable(schedule: Schedule): boolean {
    for (let i = 0; i < schedule.operationSequence.length; i++) {
      const runningOperation = schedule.operationSequence[i];
      if (isItemOperation(runningOperation) && runningOperation.operationType === OperationType.Write) {
        const runningTransactionClosingOp = schedule.closingOperationSequence.find(
          transaction => transaction.transactionId === runningOperation.transactionId,
        );
        if (runningTransactionClosingOp?.operationType === OperationType.Abort) {
          if (
            this.hasAbortConflict(
              [...schedule.closingOperationSequence],
              runningOperation,
              schedule.operationSequence.slice(i + 1),
            )
          ) {
            return false;
          }
        } else if (
          this.hasConflictsInFollowingOps(
            [...schedule.closingOperationSequence],
            runningOperation,
            schedule.operationSequence.slice(i + 1),
          )
        ) {
          return false;
        }
      }
    }

    return true;
  }

  private hasAbortConflict(
    closingOpSequence: Array<ClosingOperation>,
    checkOp: Operation,
    operations: Array<Operation>,
  ) {
    for (const runningOp of operations) {
      if (runningOp.operationType === OperationType.Abort && checkOp.transactionId === runningOp.transactionId) {
        return false;
      }

      const checkTransactionClosingOp = closingOpSequence.find(
        operation => operation.transactionId === checkOp.transactionId,
      );

      const runningTransactionClosingOp = closingOpSequence.find(
        operation => operation.transactionId === runningOp.transactionId,
      );

      if (
        runningOp.transactionId !== checkOp.transactionId &&
        runningTransactionClosingOp.operationType === OperationType.Abort &&
        checkTransactionClosingOp.operationType === OperationType.Abort
      ) {
        const checkClosingIndex = closingOpSequence.findIndex(
          operation => operation.transactionId === checkOp.transactionId,
        );
        const runningClosingIndex = closingOpSequence.findIndex(
          operation => operation.transactionId === runningOp.transactionId,
        );

        if (checkClosingIndex < runningClosingIndex) {
          return true;
        }
      } else if (
        isItemOperation(runningOp) &&
        isItemOperation(checkOp) &&
        runningOp.transactionId !== checkOp.transactionId &&
        runningOp.item === checkOp.item
      ) {
        return true;
      }
    }

    return false;
  }

  private hasConflictsInFollowingOps(
    closingOpSequence: Array<ClosingOperation>,
    checkOp: Operation,
    operations: Array<Operation>,
  ): boolean {
    for (const runningOp of operations) {
      if (isClosingOperation(runningOp) && checkOp.transactionId === runningOp.transactionId) {
        return false;
      }

      const runningTransactionClosingOp = closingOpSequence.find(
        operation => operation.transactionId === runningOp.transactionId,
      );

      if (
        isItemOperation(checkOp) &&
        isItemOperation(checkOp) &&
        isItemOperation(checkOp) &&
        isItemOperation(runningOp) &&
        checkOp.transactionId !== runningOp.transactionId &&
        runningTransactionClosingOp?.operationType !== OperationType.Abort &&
        checkOp.item === runningOp.item
      ) {
        const checkClosingIndex = closingOpSequence.findIndex(
          operation => operation.transactionId === checkOp.transactionId,
        );
        const runningClosingIndex = closingOpSequence.findIndex(
          operation => operation.transactionId === runningOp.transactionId,
        );

        if (checkClosingIndex > runningClosingIndex) {
          return true;
        }
      }
    }

    return false;
  }
}
