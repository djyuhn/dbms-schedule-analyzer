import { TestBed } from '@angular/core/testing';

import { RecoverabilityAnalyzerService } from './recoverability-analyzer.service';
import { Transaction } from '../../models/Transaction';
import { OperationType } from '../../models/OperationType';
import { ItemOperation } from '../../models/ItemOperation';
import { Schedule } from '../../models/Schedule';

describe('RecoverabilityAnalyzerService', () => {
  let service: RecoverabilityAnalyzerService;
  let transactionOne: Transaction;
  let transactionTwo: Transaction;
  let transactionThree: Transaction;

  beforeEach(() => {
    transactionOne = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Write, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Read, item: 2, transactionId: 1 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 1 },
      transactionId: 1,
    };
    transactionTwo = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Abort, transactionId: 2 },
      transactionId: 2,
    };
    transactionThree = {
      operations: [
        { operationType: OperationType.Read, item: 2, transactionId: 3 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 3 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 3 },
      transactionId: 3,
    };

    TestBed.configureTestingModule({});
    service = TestBed.inject(RecoverabilityAnalyzerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true given a schedule with three transactions that all commit in order', () => {
    const transactionTwoWithCommit: Transaction = {
      ...transactionTwo,
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
    };
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo, transactionThree],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwoWithCommit.operations[0],
        transactionTwoWithCommit.operations[1],
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionOne.closingOperation,
        transactionTwoWithCommit.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [
        transactionOne.closingOperation,
        transactionTwoWithCommit.closingOperation,
        transactionThree.closingOperation,
      ],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeTrue();
  });

  it('should return false given a schedule where transaction, 3, reads from another transaction, 2, where 2 aborts after the read', () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo, transactionThree],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionThree.operations[0],
        transactionTwo.closingOperation,
        transactionThree.operations[1],
        transactionOne.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [
        transactionTwo.closingOperation,
        transactionOne.closingOperation,
        transactionThree.closingOperation,
      ],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeFalse();
  });

  it('should return true given a schedule where transaction, 3, reads from another transaction, 2, where 2 aborts before the read', () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo, transactionThree],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionOne.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [
        transactionTwo.closingOperation,
        transactionOne.closingOperation,
        transactionThree.closingOperation,
      ],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeTrue();
  });

  it('should return false given a schedule where transactions commit out of order', () => {
    const transactionTwoWithCommit: Transaction = {
      ...transactionTwo,
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
    };
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwoWithCommit, transactionThree],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwoWithCommit.operations[0],
        transactionTwoWithCommit.operations[1],
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionThree.closingOperation,
        transactionTwoWithCommit.closingOperation,
        transactionOne.closingOperation,
      ],
      closingOperationSequence: [
        transactionThree.closingOperation,
        transactionTwoWithCommit.closingOperation,
        transactionOne.closingOperation,
      ],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeFalse();
  });

  it(`should return false given a schedule where transaction 1 reads from transaction 2's write and commits after 2 aborts`, () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.closingOperation,
        transactionOne.closingOperation,
      ],
      closingOperationSequence: [transactionTwo.closingOperation, transactionOne.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeFalse();
  });

  it(`should return false given a schedule where transaction 1 reads from transaction 2's write and commits before 2 aborts`, () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeFalse();
  });

  it(`should return true given a schedule where transaction 2 reads from transaction 1's write and aborts before 1 aborts`, () => {
    const transactionOneAborts: Transaction = {
      ...transactionOne,
      closingOperation: { operationType: OperationType.Abort, transactionId: 1 },
    };
    const schedule: Schedule = {
      transactionList: [transactionOneAborts, transactionTwo],
      operationSequence: [
        transactionOneAborts.operations[0],
        transactionOneAborts.operations[1],
        transactionOneAborts.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
        transactionOneAborts.closingOperation,
      ],
      closingOperationSequence: [transactionTwo.closingOperation, transactionOneAborts.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeTrue();
  });

  it(`should return false given a schedule where transaction 2 reads from transaction 1's write and aborts after 1 aborts`, () => {
    const transactionOneAborts: Transaction = {
      ...transactionOne,
      closingOperation: { operationType: OperationType.Abort, transactionId: 1 },
    };
    const schedule: Schedule = {
      transactionList: [transactionOneAborts, transactionTwo],
      operationSequence: [
        transactionOneAborts.operations[0],
        transactionOneAborts.operations[1],
        transactionOneAborts.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOneAborts.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOneAborts.closingOperation, transactionTwo.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeFalse();
  });

  it(`should return true given a schedule where transaction 2 reads from transaction 1's write and aborts before 1 commits`, () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
        transactionOne.closingOperation,
      ],
      closingOperationSequence: [transactionTwo.closingOperation, transactionOne.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeTrue();
  });

  it(`should return true given a schedule where transaction 2 reads from transaction 1's write and aborts after 1 commits`, () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isRecoverable = service.isRecoverable(schedule);

    expect(isRecoverable).toBeTrue();
  });
});
