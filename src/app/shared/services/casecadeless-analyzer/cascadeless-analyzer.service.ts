import { Injectable } from '@angular/core';
import { Schedule } from '../../models/Schedule';
import { Operation } from '../../models/Operation';
import { ItemOperation } from '../../models/ItemOperation';
import { OperationType } from '../../models/OperationType';
import { isClosingOperation, isItemOperation } from '../../utilities';

@Injectable({
  providedIn: 'root',
})
export class CascadelessAnalyzerService {
  public constructor() {}

  public isCascadeless(schedule: Schedule) {
    for (let i = 0; i < schedule.operationSequence.length; i++) {
      const runningOperation = schedule.operationSequence[i];
      if (isItemOperation(runningOperation) && runningOperation.operationType === OperationType.Write) {
        const hasConflict = this.hasConflict(runningOperation, schedule.operationSequence.slice(i + 1));
        if (hasConflict) {
          return false;
        }
      }
    }

    return true;
  }

  private hasConflict(checkOp: ItemOperation, followingOperations: Operation[]): boolean {
    for (const runOp of followingOperations) {
      if (isItemOperation(runOp)) {
        if (this.checkReadConflict(checkOp, runOp)) {
          return true;
        }

        if (this.checkRewriteToDateItem(checkOp, runOp)) {
          return false;
        }
      }

      if (isClosingOperation(runOp)) {
        if (runOp.transactionId === checkOp.transactionId) {
          return false;
        }
      }
    }

    return false;
  }

  private checkReadConflict(operationOne: ItemOperation, operationTwo: ItemOperation) {
    return (
      operationOne.operationType === OperationType.Write &&
      operationOne.item === operationTwo.item &&
      operationTwo.operationType === OperationType.Read
    );
  }

  private checkRewriteToDateItem(operationOne: ItemOperation, operationTwo: ItemOperation) {
    return (
      operationOne.transactionId === operationTwo.transactionId &&
      operationOne.operationType === OperationType.Write &&
      operationOne.item === operationTwo.item
    );
  }
}
