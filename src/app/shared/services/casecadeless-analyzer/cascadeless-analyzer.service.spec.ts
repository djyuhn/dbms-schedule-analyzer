import { TestBed } from '@angular/core/testing';

import { CascadelessAnalyzerService } from './cascadeless-analyzer.service';
import { Transaction } from '../../models/Transaction';
import { OperationType } from '../../models/OperationType';
import { ItemOperation } from '../../models/ItemOperation';
import { Schedule } from '../../models/Schedule';

describe('CascadelessAnalyzerService', () => {
  let service: CascadelessAnalyzerService;
  let transactionOne: Transaction;
  let transactionTwo: Transaction;
  let transactionThree: Transaction;

  beforeEach(() => {
    transactionOne = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Write, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Read, item: 2, transactionId: 1 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 1 },
      transactionId: 1,
    };
    transactionTwo = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Abort, transactionId: 2 },
      transactionId: 2,
    };
    transactionThree = {
      operations: [
        { operationType: OperationType.Read, item: 2, transactionId: 3 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 3 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 3 },
      transactionId: 3,
    };

    TestBed.configureTestingModule({});
    service = TestBed.inject(CascadelessAnalyzerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true given a schedule with two transactions where transaction 2 reads conflicting operation of transaction 1 after transaction 1 commits', () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeTrue();
  });

  it('should return false given a schedule with two transactions where transaction 2 reads conflicting operation of transaction 1 before transaction 1 commits', () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeFalse();
  });

  it('should return true given a schedule with two transactions where transaction 2 writes conflicting operation of transaction 1 before transaction 1 commits', () => {
    const transactionTwoWithWrites: Transaction = {
      operations: [
        { operationType: OperationType.Write, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwoWithWrites],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwoWithWrites.operations[0],
        transactionTwoWithWrites.operations[1],
        transactionOne.closingOperation,
        transactionTwoWithWrites.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwoWithWrites.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeTrue();
  });

  it('should return false given a schedule with three transactions where transaction 3 reads from conflicting operation of transaction 2 before transaction 2 commits and transaction 2 writes to conflicting operation of transaction 1 before transaction 1 commits', () => {
    const transactionTwoWithWrites: Transaction = {
      operations: [
        { operationType: OperationType.Write, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwoWithWrites],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwoWithWrites.operations[0],
        transactionTwoWithWrites.operations[1],
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionOne.closingOperation,
        transactionTwoWithWrites.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwoWithWrites.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeFalse();
  });

  it('should return false given a schedule with three transactions where transaction 3 reads from conflicting operation of transaction 2 before transaction 2 commits and transaction 2 reads conflicting operation of transaction 1 after transaction 1 commits', () => {
    const transactionTwoWithReads: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwoWithReads],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionTwoWithReads.operations[0],
        transactionTwoWithReads.operations[1],
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionTwoWithReads.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwoWithReads.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeFalse();
  });

  it('should return true given a schedule with three transactions where transaction 3 reads from conflicting operation of transaction 2 after transaction 2 aborts and transaction 2 reads conflicting operation of transaction 1 after transaction 1 commits', () => {
    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isCascadeless = service.isCascadeless(schedule);

    expect(isCascadeless).toBeTrue();
  });
});
