import { TestBed } from '@angular/core/testing';

import { SerializabilityAnalyzerService } from './serializability-analyzer.service';
import { OperationType } from '../../models/OperationType';
import { Schedule } from '../../models/Schedule';
import { Transaction } from '../../models/Transaction';
import { ItemOperation } from '../../models/ItemOperation';

describe('SerializabilityAnalyzerService', () => {
  let service: SerializabilityAnalyzerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SerializabilityAnalyzerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true given a schedule with two transactions is serializable', () => {
    const transactionOne: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Write, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Read, item: 2, transactionId: 1 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 1 },
      transactionId: 1,
    };
    const transactionTwo: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionOne.operations[2],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isSerializable = service.isSerializable(schedule);

    expect(isSerializable).toBeTrue();
  });

  it('should return false given a schedule with two transactions that are not serializable', () => {
    const transactionOne: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Read, item: 2, transactionId: 1 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 1 },
      transactionId: 1,
    };
    const transactionTwo: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionTwo.closingOperation,
      ],
      closingOperationSequence: [transactionOne.closingOperation, transactionTwo.closingOperation],
    };

    const isSerializable = service.isSerializable(schedule);

    expect(isSerializable).toBeFalse();
  });

  it('should return true given a schedule with four transactions that are serializable', () => {
    const transactionOne: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Write, item: 1, transactionId: 1 } as ItemOperation,
        { operationType: OperationType.Read, item: 2, transactionId: 1 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 1 },
      transactionId: 1,
    };
    const transactionTwo: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 2 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 2 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const transactionThree: Transaction = {
      operations: [
        { operationType: OperationType.Read, item: 1, transactionId: 3 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 3 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const transactionFour: Transaction = {
      operations: [
        { operationType: OperationType.Write, item: 1, transactionId: 4 } as ItemOperation,
        { operationType: OperationType.Write, item: 2, transactionId: 4 } as ItemOperation,
        { operationType: OperationType.Read, item: 1, transactionId: 4 } as ItemOperation,
      ],
      closingOperation: { operationType: OperationType.Commit, transactionId: 2 },
      transactionId: 2,
    };

    const schedule: Schedule = {
      transactionList: [transactionOne, transactionTwo, transactionThree, transactionFour],
      operationSequence: [
        transactionOne.operations[0],
        transactionOne.operations[1],
        transactionTwo.operations[0],
        transactionTwo.operations[1],
        transactionTwo.closingOperation,
        transactionOne.operations[2],
        transactionOne.closingOperation,
        transactionThree.operations[0],
        transactionThree.operations[1],
        transactionFour.operations[0],
        transactionFour.operations[1],
        transactionFour.operations[2],
        transactionFour.closingOperation,
        transactionThree.closingOperation,
      ],
      closingOperationSequence: [
        transactionTwo.closingOperation,
        transactionOne.closingOperation,
        transactionFour.closingOperation,
        transactionThree.closingOperation,
      ],
    };

    const isSerializable = service.isSerializable(schedule);

    expect(isSerializable).toBeTrue();
  });
});
