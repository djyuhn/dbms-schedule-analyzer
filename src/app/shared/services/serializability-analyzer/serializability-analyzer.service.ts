import { Injectable } from '@angular/core';
import createGraph, { Graph } from 'ngraph.graph';
import { Schedule } from '../../models/Schedule';
import { Operation } from '../../models/Operation';
import { OperationType } from '../../models/OperationType';
import { ItemOperation } from '../../models/ItemOperation';
import { GraphingService } from '../graphing/graphing.service';
import { isClosingOperation, isItemOperation } from '../../utilities';

@Injectable({
  providedIn: 'root',
})
export class SerializabilityAnalyzerService {
  public constructor(private readonly graphingService: GraphingService) {}

  public isSerializable(schedule: Schedule): boolean {
    let graph: Graph = createGraph();
    schedule.transactionList.forEach(transaction => {
      graph.addNode(transaction.transactionId);
    });

    if (graph.getNodeCount.length === 1) {
      return true;
    }

    for (let i = 1; i < schedule.operationSequence.length; i++) {
      const runningOperation = schedule.operationSequence[i];
      if (isItemOperation(runningOperation)) {
        if (runningOperation.operationType === OperationType.Read) {
          graph = this.handleRead(runningOperation, schedule.operationSequence.slice(i + 1), graph);
        } else {
          graph = this.handleWrite(runningOperation, schedule.operationSequence.slice(i + 1), graph);
        }
      } else if (isClosingOperation(runningOperation)) {
        graph.removeNode(runningOperation.transactionId);
      }

      const isNotSerializable = this.graphingService.graphHasCycle(graph);
      if (isNotSerializable) {
        return false;
      }
    }

    return true;
  }

  private handleRead(operation: ItemOperation, followingOperations: ReadonlyArray<Operation>, graph: Graph): Graph {
    for (const runningOperation of followingOperations) {
      if (isClosingOperation(runningOperation) && runningOperation.transactionId === operation.transactionId) {
        return graph;
      }
      if (
        isItemOperation(runningOperation) &&
        runningOperation.operationType === OperationType.Write &&
        runningOperation.transactionId !== operation.transactionId
      ) {
        graph.addLink(operation.transactionId, runningOperation.transactionId);
      }
    }

    return graph;
  }

  private handleWrite(operation: ItemOperation, followingOperations: ReadonlyArray<Operation>, graph: Graph): Graph {
    for (const runningOperation of followingOperations) {
      if (isClosingOperation(runningOperation) && runningOperation.transactionId === operation.transactionId) {
        return graph;
      }
      if (isItemOperation(runningOperation) && runningOperation.transactionId !== operation.transactionId) {
        graph.addLink(operation.transactionId, runningOperation.transactionId);
      }
    }

    return graph;
  }
}
