import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { AboutComponent } from './pages/about/about.component';
import { HomeModuleComponent } from './home-module.component';

@NgModule({
  declarations: [HomeModuleComponent, HomeComponent, AboutComponent],
  imports: [CommonModule, HomeRoutingModule],
})
export class HomeModule {}
