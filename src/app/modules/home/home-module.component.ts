import { Component, OnInit } from '@angular/core';
import { homeAnimations } from './animations/home-animations';
import { Link, NavbarService } from '../../shared/services/navbar/navbar.service';

@Component({
  selector: 'dbms-home-module',
  templateUrl: './home-module.component.html',
  styleUrls: ['./home-module.component.scss'],
  animations: [homeAnimations],
})
export class HomeModuleComponent implements OnInit {
  private links: ReadonlyArray<Link> = [
    { path: '/home', text: 'Home' },
    { path: '/about', text: 'About' },
    { path: '/chapter-one', text: 'Chapter One' },
  ];

  public constructor(private navbarService: NavbarService) {}

  public ngOnInit(): void {
    this.navbarService.setNavLinks([...this.links]);
    this.navbarService.showNav();
    this.navbarService.hideSubNav();
  }
}
