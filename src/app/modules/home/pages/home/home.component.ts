import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'dbms-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public constructor(private router: Router) {}

  public ngOnInit(): void {}

  public handleStartClick() {
    this.router.navigate(['/chapter-one']);
  }
}
