import { animate, group, query, style, transition, trigger } from '@angular/animations';

const slideDown = [
  query(':enter, :leave', style({ position: 'absolute', height: '100%' }), { optional: true }),
  group([
    query(
      ':leave',
      [style({ transform: 'translateY(0%)' }), animate('1s ease-in-out', style({ transform: 'translateY(100%)' }))],
      { optional: true },
    ),
    query(
      ':enter',
      [style({ transform: 'translateY(-100%)' }), animate('1s ease-in-out', style({ transform: 'translateY(0%)' }))],
      { optional: true },
    ),
  ]),
];

const slideUp = [
  query(':enter, :leave', style({ position: 'absolute', height: '100%' }), { optional: true }),
  group([
    query(
      ':leave',
      [
        style({ transform: 'translateY(0%)' }),
        animate(
          '1s ease-in-out',
          style({
            transform: 'translateY(-100%)',
          }),
        ),
      ],
      { optional: true },
    ),
    query(
      ':enter',
      [
        style({ transform: 'translateY(100%)' }),
        animate(
          '1s ease-in-out',
          style({
            transform: 'translateY(0%)',
          }),
        ),
      ],
      { optional: true },
    ),
  ]),
];

const fadeOutFadeIn = [
  query(':enter, :leave', style({ position: 'absolute', height: '100%' })),
  query(':leave', [style({ opacity: 1 }), animate('1s', style({ opacity: 0 }))], { optional: true }),
  query(':enter', [style({ opacity: 0 }), animate('1s', style({ opacity: 1 }))], { optional: true }),
];

export const homeAnimations = trigger('routeAnimations', [
  transition('void => *', fadeOutFadeIn),
  transition('Home => About', slideUp),
  transition('About => Home', slideDown),
]);
