import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChapterOneRoutingModule } from './chapter-one-routing.module';
import { WriteOperationComponent } from './pages/write-operation/write-operation.component';
import { ChapterOneComponent } from './chapter-one.component';
import { OperationsComponent } from './pages/operations/operations.component';
import { SubNavComponent } from './components/sub-nav/sub-nav.component';

@NgModule({
  declarations: [ChapterOneComponent, WriteOperationComponent, OperationsComponent, SubNavComponent],
  imports: [CommonModule, ChapterOneRoutingModule],
})
export class ChapterOneModule {}
