import { Component, OnInit } from '@angular/core';

import { Link, NavbarService } from '../../shared/services/navbar/navbar.service';
import { chapterOneAnimations } from './animations/chapter-one-animations';

@Component({
  selector: 'dbms-chapter-one',
  templateUrl: './chapter-one.component.html',
  styleUrls: ['./chapter-one.component.scss'],
  animations: [chapterOneAnimations],
})
export class ChapterOneComponent implements OnInit {
  private links: ReadonlyArray<Link> = [
    { path: '/home', text: 'Home' },
    { path: '/about', text: 'About' },
    { path: '/chapter-one', text: 'Chapter One' },
  ];

  private subnavLinks: ReadonlyArray<Link> = [
    { path: '/chapter-one/operations', text: 'Operations' },
    { path: '/chapter-one/write-operation', text: 'Write Operation' },
  ];

  public constructor(private navbarService: NavbarService) {}

  public ngOnInit(): void {
    this.navbarService.setNavLinks([...this.links]);
    this.navbarService.setSubnavLinks([...this.subnavLinks]);
    this.navbarService.showNav();
    this.navbarService.showSubNav();
  }
}
