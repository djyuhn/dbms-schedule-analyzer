import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteOperationComponent } from './write-operation.component';

describe('WriteOperationComponent', () => {
  let component: WriteOperationComponent;
  let fixture: ComponentFixture<WriteOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WriteOperationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
