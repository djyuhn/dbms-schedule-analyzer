import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ChapterOneComponent } from './chapter-one.component';
import { OperationsComponent } from './pages/operations/operations.component';
import { WriteOperationComponent } from './pages/write-operation/write-operation.component';

export const routes: Routes = [
  {
    path: '',
    component: ChapterOneComponent,
    children: [
      {
        path: '',
        redirectTo: 'operations',
        pathMatch: 'full',
      },
      {
        path: 'operations',
        component: OperationsComponent,
        data: { animation: 'Operations' },
      },
      {
        path: 'write-operation',
        component: WriteOperationComponent,
        data: { animation: 'WriteOperation' },
      },
    ],
  },
  { path: '**', redirectTo: 'operations', pathMatch: 'full' },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChapterOneRoutingModule {}
